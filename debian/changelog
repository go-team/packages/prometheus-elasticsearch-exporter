prometheus-elasticsearch-exporter (1.9.0-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Refresh packaging according to latest dh-make-golang template
  * Bump Standards-Version to 4.7.2 (no changes)
  * Drop obsolete 0002-Support-prometheus-common-v0.50.0.patch (applied
    upstream)
  * Refresh remaining patches
  * Drop unused Build-Depends golang-github-go-kit-log-dev
  * Bump golang-github-prometheus-exporter-toolkit-dev version constraint
    (>= 0.13.0)

 -- Daniel Swarbrick <dswarbrick@debian.org>  Mon, 03 Mar 2025 16:18:58 +0000

prometheus-elasticsearch-exporter (1.8.0-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Bump Standards-Version to 4.7.0 (no changes)
  * Refresh patches

 -- Daniel Swarbrick <dswarbrick@debian.org>  Mon, 23 Sep 2024 13:53:26 +0000

prometheus-elasticsearch-exporter (1.7.0-2) unstable; urgency=medium

  * Team upload
  * Add new 0002-Support-prometheus-common-v0.50.0.patch
  * Bump golang-github-prometheus-client-golang-dev version constraint
    (>= 1.19.0)

 -- Daniel Swarbrick <dswarbrick@debian.org>  Mon, 18 Mar 2024 04:38:19 +0000

prometheus-elasticsearch-exporter (1.7.0-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Modernise debian/rules to generate manpage from exporter itself

 -- Daniel Swarbrick <dswarbrick@debian.org>  Mon, 15 Jan 2024 00:29:02 +0000

prometheus-elasticsearch-exporter (1.6.0-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Add new Revert-alecthomas-kingpin-import-path.patch
  * Add new build-dep golang-github-prometheus-exporter-toolkit-dev

 -- Daniel Swarbrick <dswarbrick@debian.org>  Tue, 04 Jul 2023 19:30:21 +0000

prometheus-elasticsearch-exporter (1.5.0-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Add new build-dep golang-github-aws-aws-sdk-go-v2-dev

 -- Daniel Swarbrick <dswarbrick@debian.org>  Mon, 30 Jan 2023 23:23:22 +0000

prometheus-elasticsearch-exporter (1.4.0-2) unstable; urgency=medium

  * Team upload
  * Fix debian/watch, based on dh-make-golang template
  * Bump Standards-Version to 4.6.2 (no changes)
  * Remove --help output from default file

 -- Daniel Swarbrick <dswarbrick@debian.org>  Tue, 10 Jan 2023 00:00:56 +0000

prometheus-elasticsearch-exporter (1.4.0-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Drop +ds suffix, since we no longer need to unvendor
  * Drop obsolete patches
    - 01-fix-http-handler.patch
    - 02-fix-flaky-test.patch
  * Update upstream URLs and XS-Go-Import-Path
  * Update copyright attribution
  * Replace golang-github-go-kit-kit-dev build-dep with
    golang-github-go-kit-log-dev
  * Bump Standards-Version to 4.6.1 (no changes)
  * Replace dh-golang with dh-sequence-golang
  * Replace golang-go build-dep with golang-any

 -- Daniel Swarbrick <dswarbrick@debian.org>  Mon, 24 Oct 2022 18:48:52 +0000

prometheus-elasticsearch-exporter (1.1.0+ds-2) unstable; urgency=medium

  * Team upload.
  * No change upload to rebuild on buildd.

 -- Benjamin Drung <benjamin.drung@cloud.ionos.com>  Thu, 04 Feb 2021 17:25:04 +0100

prometheus-elasticsearch-exporter (1.1.0+ds-1) unstable; urgency=medium

  * Initial release. (Closes: #935317)

 -- Badreddin Aboubakr <badreddin.aboubakr@cloud.ionos.com>  Mon, 25 Jan 2021 21:21:21 +0100
